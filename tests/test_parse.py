from summarist import *

char_set = []
for i in range(ord('a'), ord('z') + 1):
  char_set.append(chr(i))

for i in range(ord('A'), ord('Z') + 1):
  char_set.append(chr(i))

for i in range(ord('0'), ord('9') + 1):
  char_set.append(chr(i))

char_set.extend('.,/!?\n@$&*\t -+=~(){}[]\\`\'\"')

def random_text():
  import random
  length = random.randrange(128, 1024)

  return ''.join(random.choices(char_set, k=length))

def random_line():
  import random
  length = random.randrange(64, 256)

  return ''.join(random.choices(char_set, k=length)).replace('\n', ' ')

def compare(expected, result, source):
  try:
    expected = expected.strip()
    result = result.strip()
    assert result == expected

  except:
    print()
    print(source)
    print()
    print('>>>>>>>>>>>')
    print(result)
    print('===========')
    print(expected)
    print('<<<<<<<<<<<')
    raise

def test_block():
  for _ in range(1024):
    pre_text = random_text().strip()
    text = random_text().strip()
    post_text = random_text().strip()
    source = '%s\n```abstract\n%s\n```\n%s' % (pre_text, text, post_text)

    compare(text, get_abstract_block(source), source)

def test_tex():
  for _ in range(1024):
    pre_text = random_text().strip()
    text = random_text().strip()
    post_text = random_text().strip()
    source = '%s\n\\begin{abstract}\n%s\n\\end{abstract}\n%s' % (pre_text, text, post_text)

    compare(text, get_abstract_tex(source), source)

def test_tex_document():
  for _ in range(1024):
    pre_text = random_text().strip()
    text = random_text().strip()
    post_text = random_text().strip()
    source = '%s\n\\begin{document}\n%s\n\\end{document}\n%s' % (pre_text, text, post_text)

    compare(text.strip()[:128], get_abstract_tex_document(source), source)

def test_md():
  for _ in range(1024):
    pre_text = random_text().strip()
    header = random_line().strip()
    section = random_line().strip()
    text = random_text().strip()
    post_text = random_text().strip()
    source = '%s\n# %s\n%s\n## %s\n%s' % (pre_text, header, text, section, post_text)

    compare(text, get_abstract_md(source), source)

def test_md2():
  for _ in range(1024):
    pre_text = random_text().strip()
    header = random_line().strip()
    text = random_text().strip()
    source = '%s\n# %s\n%s' % (pre_text, header, text)

    compare(text, get_abstract_md(source), source)
