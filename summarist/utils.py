import os
import re

__all__ = [
  'cut_string',

  'get_header_md',
  'get_first_line_md',
  'get_header_tex',

  'get_abstract_block',
  'get_abstract_md',

  'get_abstract_tex',
  'get_abstract_tex_document',

  'get_info'
]

def cut_string(string, limit=1024, max_lines=5):
  string = string.strip()

  if len(string) >= limit:
    string = '%s...' % (string[:(limit - 3)], )

  lines = string.split('\n')
  return '\n'.join(lines[:max_lines])

def capture(source, reg_exp):
  match = reg_exp.search(source)
  if match is None:
    return None
  else:
    return match.group('body')

md_header = re.compile('#(?P<body>[^\\n#][^\\n]*)\\n')

def get_header_md(source):
  return capture(source, md_header)

def get_first_line_md(source):
  return source.split('\n')[0]

tex_header = re.compile('\\\\title\\{(?P<body>.*?)\\}', re.DOTALL)

def get_header_tex(source):
  return capture(source, tex_header)

block_abstract = re.compile(
  '(?<=\\n)[ \\t]*```abstract[ \\t]*\\n(?P<body>.*?)\\n[ \\t]*```',
  re.DOTALL
)

def get_abstract_block(source):
  return capture(source, block_abstract)

md_abstract = re.compile(
  '#.*?\\n(?P<body>.*?)(\\n[ \\t]*#|$)',
  re.DOTALL
)

def get_abstract_md(source):
  return capture(source, md_abstract)

tex_abstract = re.compile(
  '\\\\begin\\{abstract\\}(?P<body>.*?)\\\\end\\{abstract\\}',
  re.DOTALL
)

def get_abstract_tex(source):
  return capture(source, tex_abstract)

tex_document_abstract = re.compile(
  '\\\\begin\\{document\\}\s*(?P<body>.{0,128})',
  re.DOTALL
)

def get_abstract_tex_document(source):
  return capture(source, tex_document_abstract)

ABSTRACT_RULES = {
  'abstract.md' : [get_abstract_md],
  'README.md' : [get_abstract_block, get_abstract_md],
  'README.tex' : [get_abstract_tex, get_abstract_tex_document],
  None : [lambda directory: None],
}

HEADER_RULES = {
  'abstract.md' : [get_header_md, get_first_line_md],
  'README.md' : [get_header_md, get_first_line_md],
  'README.tex' : [get_header_tex, ],
  None : [lambda directory: os.path.basename(directory)]
}

ACTIVE_FILE = {
  'abstract.md': [lambda _: 'abstract.md'],
  'README.md': [lambda _: 'README.md'],
  'README.tex': [lambda _: 'README.tex'],
  None: [lambda directory: os.path.basename(directory)]
}

def apply_rule(fs, x):
  for f in fs:
    result = f(x)
    if result is not None:
      return result

  raise IndexError('No rules')


def apply_rules(directory, *rules):
  default_rule = rules[0]

  for pattern in default_rule:
    if pattern is None:
      continue

    path = os.path.join(directory, pattern)
    if not os.path.exists(path):
      continue

    with open(path, 'r') as f:
      source = f.read()

    try:
      return tuple(
        apply_rule(rule[pattern], source)
        for rule in rules
      )
    except:
      continue

  return tuple(
    apply_rule(rule[None], directory)
    for rule in rules
  )

def get_info(directory):
  return apply_rules(directory, HEADER_RULES, ABSTRACT_RULES)