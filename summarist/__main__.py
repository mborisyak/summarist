import os
import re

from string import Template

from .utils import *

def main(
  root, template='README.template.md', target='README.md',
  max_abstract_len=1024, max_abstract_lines=5, header_level=3
):
  header_level = '#' * header_level

  non_alpha = re.compile('[^\\w ]')
  results = []

  for item in os.listdir(root):
    path = os.path.join(root, item)

    if os.path.isdir(path):
      try:
        header, abstract = get_info(path)
      except IndexError:
        print('Can not obtain info from %s' % (path, ))


      header = item if header is None else header
      abstract = '<no abstract>' if abstract is None else abstract

      header = cut_string(header, limit=128, max_lines=1)
      abstract = cut_string(abstract, limit=max_abstract_len, max_lines=max_abstract_lines)

      header_link = non_alpha.sub('', header).replace(' ', '-').lower()
      header_link = header_link

      results.append((header, header_link, item, abstract))

  content = '\n\n'.join(
    '{level} [{header}]({url})\n'
    '{abstract}'.format(
      level=header_level,
      header=header,
      abstract=abstract,
      url=file
    )
    for header, _, file, abstract in results
  )

  table_of_content = '\n'.join(
    '- [{header}](#{anchor})'.format(
      header=header, anchor=header_link
    )
    for header, header_link, _, _ in results
  )

  with open(os.path.join(root, template), 'r') as f:
    template = Template(f.read())

  target_path = os.path.join(root, target)

  with open(target_path, 'w') as f:
    f.write(
      template.safe_substitute(content=content, table=table_of_content)
    )

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser(prog='summarist')
  parser.add_argument('root', type=str)
  parser.add_argument('--template', type=str, default='README.template.md')
  parser.add_argument('--target', type=str, default='README.md')
  parser.add_argument('--max-len', type=int, default=1024)
  parser.add_argument('--max-lines', type=int, default=5)
  parser.add_argument('--header-level', type=int, default=3)

  args = parser.parse_args()

  main(args.root, args.template, args.target, args.max_len, args.max_lines, args.header_level)