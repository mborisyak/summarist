# summarist

A python script that collects abstracts from subdirectories and compiles then into a single README.md

## Usage

```bash
python -m summarist <root>/ \
  --target=<target file> \ #default README.md
  --template=<template file> \ #default README.template.md
  --max-len=<maximum abstract length> \ #default 1024
  --max-lines=<maximum number of lines for asbtract> \ #default 5
  --header-level=<header level for list of the projects> # #default 3
```

## How it works

Summarist scans subdirectories of `<root>` looking for
`abstract.md`, `README.md` or `README.tex` (in this order).
If one of the files exists, it tries to extract title and abstract of the document
using the following rules:

**Title**
- `abstract.md` and `README.md`:
  - level-1 header as title;
  - the first line;
- `README.tex`:
  - argument of `\title` command, e.g. `\title{My great title}`
    - it does not parses TeX and does not count braces, thus, `\title{My great title with \LaTeX{} and more}`
    will result in title `My great title with \LaTeX{`:  
    - hack: if you are using document class without `\title` command
    (you so crazy that you are using LaTeX commands in the title) you can comment it:
    ```latex
    %\title{My poor commented title}
    ```
- otherwise, the name of the subdirectory is used, e.g. for subfolder `<root>/my-project`
  `my-project` is used as the title.

**Abstract**
- `abstract.md`:
  - everything between first level-1 header and the first level-2 header after the first one:
  ```md
  Who does write before title?
  # Proposal to extend ZFC
  In this work we solve Fermat's theorems (all of them) with a simple trick.
  ## Intro
  ```  
- `README.md`:
  - code blocks with `abstract` language like (for compatibility with [mdslides](https://gitlab.com/mborisyak/mdslides/)):
  ~~~
  ```abstract
  In this document, we propose a super cool method to solve world hunger.
  ```
  ~~~
  - the same as for `abstract.md`.
- `README.tex`:
  - everything wrapped in `\begin{abstract}` and `\end{abstract}`;
  - hack: if document class does not have abstract environment,
  [you can define it](https://www.overleaf.com/learn/latex/Environments#Defining_environments_with_parameters):
  ```latex
  \newenvironment{abstract}
  {\iffalse}
  {\fi}
  ```
  - the first 128 symbols after `\begin{document}`;
- text `<no abstract>` is used otherwise.

## Templates

After collecting all title and abstracts summarist generates
the table of content and list of all projects with abstracts.

Then, it substitutes `$table` and `$content` in the [template file](https://docs.python.org/3/library/string.html#template-strings)
(`--template=<template file>`, default: `README.template.md`),
and writes the result into the target file `<root>/<target file>`
(`--target=<target file>`, default: `README.md`). 

An example output is provided below. 

## $table

- [Proposal to extend ZFC](#proposal-to-extend-zfc)
- [World hunger solution](#world-hunger-solution)

## $content

### [World hunger solution](world-hunger-solution)

In this document, we propose a super cool method to solve world hunger.

### [Proposal to extend ZFC](zfc-extension)

In this work we solve Fermat's theorems (all of them) with a simple trick.

