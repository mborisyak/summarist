from setuptools import setup, find_packages
from codecs import open
import os.path as osp

here = osp.abspath(osp.dirname(__file__))

with open(osp.join(here, 'README.md'), encoding='utf-8') as f:
  long_description = f.read()

setup(
  name = 'summarist',

  version='1.0.0',

  description="""A python script that collects abstracts from subdirectories and compiles then into a single README.md""",

  long_description = long_description,

  url='https://gitlab.com/mborisyak/summarist',

  author='Maxim Borisyak and contributors.',
  author_email='mborisyak at hse dot ru',

  maintainer = 'Maxim Borisyak',
  maintainer_email = 'mborisyak at hse dot ru',

  license='MIT',

  classifiers=[
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3',
  ],

  keywords='housekeeping',

  packages=find_packages(exclude=['contrib', 'examples', 'docs', 'tests']),

  extras_require={},

  install_requires=[],
)
